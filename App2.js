import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class App2 extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.row}>
            <View style={styles.column}>
              <View style={styles.image}>
                <Text style={styles.radiusText}>Image</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.body}>
          <View style={styles.column}>
            <View style={styles.box1}>
              <Text style={styles.boxText1}>Box</Text>
            </View>
            <View style={styles.box1}>
              <Text style={styles.boxText1}>Box</Text>
            </View>
          </View>
        </View>

        <View style={styles.footer}>
          <View style={styles.box2}>
            <Text style={styles.boxText2}>Box</Text>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#666',
    flex: 1
  },
  header: {
    backgroundColor: '#666',
    flex: 2
  },
  column: {                   //row
    backgroundColor: '#666',
    flexDirection: 'column',
    alignItems: 'center',     //justifyContent
    flex: 1,
  },
  row: {                      //column
    backgroundColor: '#666',
    flexDirection: 'row',
    alignItems: 'center',     //justifyContent
    flex: 1,
  },
  image: {
    backgroundColor: 'white',
    width: 200,
    height: 200,
    borderRadius: 100,
  },
  radiusText: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 70,
  },
  body: {
    backgroundColor: 'green',
    flex: 1
  },
  box1: {
    backgroundColor: 'white',
    flexDirection: 'column',
    alignItems: 'center',
    width: 300,
    height: 60,
    borderRadius: 10,
    margin: 14,
  },
  box2: {
    backgroundColor: '#330000',
    flexDirection: 'column',
    alignItems: 'center',
    width: 300,
    height: 60,
    borderRadius: 10,
    margin: 14,
  },
  boxText1: {
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
  },
  footer: {
    backgroundColor: '#666',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
  },
  boxText2: {
    color:'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 10,
  },
});

export default App2


