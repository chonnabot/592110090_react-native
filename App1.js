import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
          <Text style={styles.headerText}>News</Text>
        </View>

        <View style={styles.content}>
        
          <View style={styles.row}>
            <View style={styles.box1}>
              <Text>Lorme</Text>
            </View>
            <View style={styles.box2}>
              <Text>Lorme</Text>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.box1}>
              <Text>Lorme</Text>
            </View>
            <View style={styles.box2}>
              <Text>Lorme</Text>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.box1}>
              <Text>Lorme</Text>
            </View>
            <View style={styles.box2}>
              <Text>Lorme</Text>
            </View>
          </View>


        </View>

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#6633FF',
    flex: 1
  },
  header: {
    backgroundColor: '#6633FF',
    alignItems: 'center',
    flexDirection: 'column',
  },
  headerText: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    padding: 30,
  },
  content: {
    backgroundColor: '#6633FF',
    flex: 1,
    flexDirection: 'column',
  },
  box1: {
    backgroundColor: '#66FF00',
    flex: 1,
    margin: 14,
  },
  box2: {
    backgroundColor: '#66FF00',
    flex: 1,
    margin: 14,
  },
  row: {
    backgroundColor: '#6633FF',
    flex: 1,
    flexDirection: 'row',
  },
});

export default App


