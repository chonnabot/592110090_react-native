import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.headerBox1}>
                        <Text style={styles.ham}></Text>
                        <Text style={styles.ham}></Text>
                        <Text style={styles.ham}></Text>
                        
                    </View>
                    <View style={styles.headerBox2}>
                        <View style={styles.headerBox1}>
                            <Text style={styles.Text}>Text</Text>
                        </View>
                    </View>
                    <View style={styles.headerBox3}>
                        <View style={styles.headerBox1}>
                            <Text style={styles.Text}>X</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.Text}>ScrollView</Text>
                </View>

                <View style={styles.footer}>
                    <View style={styles.footerBox1}>
                        <Text style={styles.Text}>I</Text>
                    </View>
                    <View style={styles.footerBox2}>
                        <Text style={styles.Text}>C</Text>
                    </View>
                    <View style={styles.footerBox2}>
                        <Text style={styles.Text}>O</Text>
                    </View>
                    <View style={styles.footerBox3}>
                        <Text style={styles.Text}>N</Text>
                    </View>
                </View>

            </View>
        );                                                  //marginHorizontal: -8
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    Text: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 10,
    },
    header: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 1,
    },
    headerBox1: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        alignItems: 'center', // เลื่อน ซ้ายไปขวา
    },
    headerBox2: {
        backgroundColor: '#66FF00',
        flex: 3,
        margin: 2,
    },
    headerBox3: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#6633FF',
        flex: 10,
        margin: 2,
        justifyContent: 'center',  //เลื่อนลงตาม row บนมาล่าง
        alignItems: 'center', // เลื่อน ซ้ายไปขวา
        marginHorizontal: -1,
    },
    footer: {
        backgroundColor: 'white',
        flexDirection: 'row',
        flex: 1,
        margin: 1,
    },
    footerBox1: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 2,
        marginLeft: -1,
        alignItems: 'center',
    },
    footerBox2: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 2,
        alignItems: 'center',
    },
    footerBox3: {
        backgroundColor: '#66FF00',
        flex: 1,
        margin: 2,
        marginRight: -1,
        alignItems: 'center',
    },
    ham:{
        width: 35,
        height: 3.5,
        backgroundColor: 'white',
        margin: 2,
    },

});

export default App


